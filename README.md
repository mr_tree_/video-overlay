<!-- PROJECT LOGO -->
<br />
  <h3>Video Overlay</h3>
    This script takes a bunch of videos and puts a timestamp as overlaying text on it.
    <br />
    <a href="https://gitlab.com/mr_tree_/video-overlay"><strong>Explore the docs »</strong></a>
    <br />
    <br />
    <a href="https://gitlab.com/mr_tree_/video-overlay">View Demo</a>
    ·
    <a href="https://gitlab.com/mr_tree_/video-overlay/issues">Report Bug</a>
    ·
    <a href="https://gitlab.com/mr_tree_/video-overlay/issues">Request Feature</a>
  </p>
</p>



<!-- TABLE OF CONTENTS -->
<details open="open">
  <summary><h2 style="display: inline-block">Table of Contents</h2></summary>
  <ol>
    <li>
      <a href="#about-the-project">About The Project</a>
      <ul>
        <li><a href="#built-with">Built With</a></li>
      </ul>
    </li>
    <li>
      <a href="#getting-started">Getting Started</a>
      <ul>
        <li><a href="#prerequisites">Prerequisites</a></li>
        <li><a href="#installation">Installation</a></li>
      </ul>
    </li>
    <li><a href="#usage">Usage</a></li>
    <li><a href="#contributing">Contributing</a></li>
    <li><a href="#license">License</a></li>
    <li><a href="#contact">Contact</a></li>
    <li><a href="#acknowledgements">Acknowledgements</a></li>
  </ol>
</details>



<!-- ABOUT THE PROJECT -->
## About The Project

This script takes all videos from a source directory called 'videos' and provides each video with an text overlay, which shows an increasing timestamp.
The timestamp starts on a manually set start time, which has to be in the filename. If there is no valid time format found, 
it will fallback to the creation time of the file.

The modified file is then stored to a destination directory, which is named 'output' by default.

You can set some options in the settings.py file, where you also can modify the source and destination folders.

### Built With

* [Python 3.9](https://www.python.org/downloads/release/python-390/)
* [OpenCV](https://opencv.org/)

<!-- GETTING STARTED -->
## Getting Started

In the following you find some instructions and examples on setting up your project locally. 

### Prerequisites

For this project, you need at least Python 3.6. If you don't have it installed yet, please download it from here and follow the instructions of the install wizard.

After you have installed Python successfully, you have to clone the repo and add some dependencies to your environment. Instructions for this are in the next section.

### Installation

1. Clone the repo
   ```sh
   git clone https://gitlab.com/mr_tree_/video-overlay.git
   ```
2. Install dependencies from requirements.txt
   ```sh
   pip install -r /path/to/requirements.txt
   ```


<!-- USAGE EXAMPLES -->
## Usage

For a fully example, you find some example files in this project structure. Just run the script and it will modify the mp4 file in 'videos' and store the final result in 'output'.

Just place your videos in the 'videos' directory, rename the files to include the starting time and run the script. A valid name should include the time in this format:

```sh
yyyy-MM-dd-hhhmmmsss
```
*Please notice: The third h, m and s are characters and no digits.*

You can add this time format everywhere in the filename and add some additional words for describing the file. For example, your filename can look like this:

```sh
Video_2021-01-25-12h00m11s (xyz).mp4
```

<!-- CONTRIBUTING -->
## Contributing

Contributions are what make the open source community such an amazing place to be learn, inspire, and create. Any contributions you make are **greatly appreciated**.

1. Fork the Project
2. Create your Feature Branch (`git checkout -b feature/AmazingFeature`)
3. Commit your Changes (`git commit -m 'Add some AmazingFeature'`)
4. Push to the Branch (`git push origin feature/AmazingFeature`)
5. Open a Pull Request


<!-- LICENSE -->
## License

Distributed under the MIT License. See `LICENSE` for more information.


<!-- CONTACT -->
## Contact

Patrick Baum - baum_patrick@web.de

Project Link: [https://gitlab.com/mr_tree_/video-overlay](https://gitlab.com/mr_tree_/video-overlay)
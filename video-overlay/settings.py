# Root paths to directories
ROOT_PATH = {
    "src": ".\\videos", 
    "dst": ".\\output"
}

# Overlay options
OVERLAY_OPTIONS = {
    "date_format": "%Y/%m/%d %H:%M:%S",
    "font_color": (0, 218, 0),
    "font_scale": 1,
    "font_style": 1,
}
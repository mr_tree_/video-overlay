""" video_overlay-0.0.3.py - Putting text overlay to multiple videos

Author:         Patrick Baum
Date:           2021-01-25
Last modified:  2021-01-26
Version:        0.0.3

The script takes all files in the same directory as the script and filters out
all files which are not video files. Afterwards, a text overlay with an increasing
timestamp is created, based on the found data in the filename or (as fallback)
depending on the creation time from the files meta info. The text overlay is put
on every frame, so the video got an increasing timestamp as subtitle.

The modified video is stored in the directory "videos mit overlay" as mp4 file.
"""
import os
import logging
import re
import datetime
from cv2 import cv2
from pymediainfo import MediaInfo
from settings import ROOT_PATH, OVERLAY_OPTIONS

# Configure logger
logging.basicConfig(
    level=logging.DEBUG, format=" %(asctime)s - %(levelname)s - %(message)s"
)


def create_video_overlay(video_path):
    """
    This function creates the video overlay. For that, each frame
    of the video is modified by putting the text overlay with the
    actual timestamp on it.

    The timestamp is increased on every iteration by the value of
    1s / frames per second.

    Things like position, color, font and font size are already set.
    You can modify them as you want.
    """
    # Get file name and creation time stamp of the file from OS
    file_name = os.path.splitext(video_path)[0]
    timestamp = get_starttime(video_path)

    # Open video
    vid = cv2.VideoCapture(video_path)

    # Get information for output about frame size and FPS
    frame_width = int(vid.get(3))
    frame_height = int(vid.get(4))
    fps = vid.get(cv2.CAP_PROP_FPS)

    # Check, if destination exists
    if not os.path.exists(ROOT_PATH["dst"]):
        logging.debug("Destination path not found. New destination directory is added.")
        os.mkdir(ROOT_PATH["dst"])

    # Create output object
    out = cv2.VideoWriter(
        "{} (with subtitle).mp4".format(
            file_name.replace(ROOT_PATH["src"], ROOT_PATH["dst"])
        ),
        0x7634706D,
        fps,
        (frame_width, frame_height),
    )

    # Put text overlay in each frame of the video
    while vid.isOpened():
        ret, frame = vid.read()
        if ret:
            # Choose the font, which is displayed with the overlay
            # font = cv2.FONT_HERSHEY_SIMPLEX
            font = cv2.FONT_HERSHEY_PLAIN

            # Calculate the timestamp according to the FPS value
            timestamp += datetime.timedelta(seconds=(1 / fps))

            # Format the timestamp and save as String value
            str_timestamp = timestamp.strftime(OVERLAY_OPTIONS["date_format"])

            # Set position, where the overlay should be displayed
            position = (10, frame_height - 20)

            # Create frame with overlay text, according to the above data
            frame = cv2.putText(
                frame,
                str_timestamp,
                position,
                font,
                OVERLAY_OPTIONS["font_scale"],
                OVERLAY_OPTIONS["font_color"],
                OVERLAY_OPTIONS["font_style"],
                cv2.LINE_AA,
            )

            # Write frame to output object
            out.write(frame)

        else:
            break

    logging.info("Video %s successfully processed!", video_path)

    # Close open streams and windows
    vid.release()
    out.release()
    cv2.destroyAllWindows()


def get_starttime(video_path):
    """
    This function extracts the correct starting time from the files name.
    For success, the time has to be in the following format:

    .*yyyy-MM-dd-hhhmmmsss.*        > Example: vnc-2021-01-25-12h00m10s-test.mp4

    If no valid date is found in the file name, there will be a fallback to the
    creation time, which is stored in the files meta info. In this case, a warning
    message is generated, so the user has the possibility to check and correct the
    wrong data.
    """
    timestamp = None
    regex = re.compile(
        "(.*)([0-9]{4})-([0-9]{2})-([0-9]{2})-([0-9]{2})h([0-9]{2})m([0-9]{2})s(.*)"
    )
    data = regex.search(video_path)

    if data is not None:
        year = int(data.group(2))
        month = int(data.group(3))
        day = int(data.group(4))
        hour = int(data.group(5))
        minute = int(data.group(6))
        second = int(data.group(7))
        timestamp = datetime.datetime(year, month, day, hour, minute, second)
    else:
        logging.warning(
            "No valid date found in name of %s. Fallback to creation date and time.",
            video_path,
        )
        timestamp = datetime.datetime.fromtimestamp(os.path.getctime(video_path))
    return timestamp


def is_video(video_path):
    """
    This function checks, if the path in the params
    is a video file or not. If not, there is a falsified
    return.
    """
    file = open(video_path, "rb")
    file_info = MediaInfo.parse(file)
    result = False
    for track in file_info.tracks:
        if track.track_type == "Video":
            result += True
        else:
            result += False
    return result


if __name__ == "__main__":
    # Create empty lists
    videos = []
    no_videos = []

    if os.path.exists(ROOT_PATH["src"]):

        # List all files from src, filter out non-video files and store both sorts of
        # files in different lists
        for path in os.listdir(ROOT_PATH["src"]):
            full_path = os.path.join(ROOT_PATH["src"], path)
            if os.path.isfile(full_path):
                if is_video(full_path):
                    videos.append(full_path)
                else:
                    no_videos.append(full_path)

        # Process all valid videos
        N_ACT = 1
        for video in videos:
            logging.info(
                "Video %s/%s [Path: %s] now processing ..", N_ACT, len(videos), video
            )
            create_video_overlay(video)
            N_ACT += 1

        # Generate warning list of all files, which are also found
        # (and maybe are in no valid format, but still videos)
        N_ACT = 1
        if len(no_videos) > 0:
            logging.warning(
                "%s%s",
                "Some files were found which are not meet the requirements for a valid format.",
                "Please check the following files:",
            )
            for no_video_file in no_videos:
                logging.warning("File %s: %s", N_ACT, no_video_file)
    else:
        logging.error(
            "Source path not found. May your in the wrong directory or your source path is missing?"
        )
